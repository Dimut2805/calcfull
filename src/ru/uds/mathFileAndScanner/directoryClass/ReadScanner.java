package ru.uds.mathFileAndScanner.directoryClass;

import ru.uds.mathFileAndScanner.directoryInterface.Read;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Scanner;

public class ReadScanner implements Read {
    /**
     * Читает строку с условием
     *
     * @return строка с условием
     */
    @Override
    public ArrayList read() {
        return new ArrayList<>(Collections.singleton(new MathObject(new Scanner(System.in).nextLine())));
    }
}
