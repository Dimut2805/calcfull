package ru.uds.mathFileAndScanner.directoryClass;

import ru.uds.mathFileAndScanner.directoryInterface.Write;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;


public class WriteFile implements Write {
    private String url;

    WriteFile(String url) {
        this.url = url;
    }

    /**
     * Пишет строки ответа в указанный файл
     *
     * @param resultBinaryExpressions массив с строками ответа
     */
    @Override
    public void writeResult(ArrayList<MathObject> resultBinaryExpressions) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(url, false))) {
            for (MathObject mathObject : resultBinaryExpressions) {
                bufferedWriter.write(mathObject.resultBinaryExpression());
            }
        } catch (IOException e) {
            System.out.println("Ошибка при работе с файлом");
        }
    }
}