package ru.uds.mathFileAndScanner.notneedclass;


import ru.uds.mathFileAndScanner.directoryClass.MathInteger;

public class MathConsider {

    /**
     * Превращает строки с условием на строки с ответом
     *
     * @param string хранит в себе строки для подсчета
     */

    public static String[] asOutput(String string) {
        String[] arrayString = string.split(System.lineSeparator());
        for (int i = 0; i < arrayString.length; i++) {
            arrayString[i] = calculator(arrayString[i].split(" "));
        }
        return arrayString;
    }

    /**
     * Подсчитывает ответ изходя из условия
     *
     * @param arguments условие
     * @return ответ из условия string
     */
    private static String calculator(String[] arguments) {
        String string = "";
        try {
            if (arguments.length != 3) throw new NumberFormatException("Неккоректный формат");
            switch (arguments[1]) {
                case "+":
                    string = String.valueOf(MathInteger.addition(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "-":
                    string = String.valueOf(MathInteger.subtraction(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "*":
                    string = String.valueOf(MathInteger.multiply(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "/":
                    string = String.valueOf(MathInteger.division(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "**":
                    string = String.valueOf(MathInteger.pow(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "%":
                    string = String.valueOf(MathInteger.divisionEnd(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
            }
        } catch (NumberFormatException | ArithmeticException e) {
            return "Ошибка! " + e.getMessage();
        } catch (Exception e) {
            return "Глобальная ошибка! " + e.getMessage();
        }
        return string;
    }
}