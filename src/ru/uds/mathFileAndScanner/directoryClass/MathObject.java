package ru.uds.mathFileAndScanner.directoryClass;

/**
 * Реализует работу с объектом
 * <p>
 * author Utin D 17ИТ18
 */
public class MathObject {
    private MathUtils mathUtils;

    MathObject(String stringBinaryExpression) {
        String[] arrayBinaryExpression = stringBinaryExpression.split(" ");
        mathUtils = new MathUtils(arrayBinaryExpression);
    }

    /**
     * Возвращает результат вычисления
     *
     * @return результат вычисления
     */
    String resultBinaryExpression() {
        return mathUtils.getA() != null &
                mathUtils.getB() != null &
                mathUtils.getSign() != null ?
                mathUtils.calculator(mathUtils.getA(), mathUtils.getSign(), mathUtils.getB()) :
                "Неккоректный формат";
    }

    /**
     * Утилиты
     */
    private static class MathUtils {
        private Long a, b;
        private Character sign;
        private static final String OPERATIONS = "+-/*%^";

        MathUtils(String[] arrayBinaryExpression) {
            if (arrayBinaryExpression.length == 3) {
                this.a = convertInLong(arrayBinaryExpression[0]);
                this.b = convertInLong(arrayBinaryExpression[2]);
                this.sign =
                        arrayBinaryExpression[1].length() == 1 ?
                                arrayBinaryExpression[1].charAt(0) :
                                null;
            }
        }

        Long getA() {
            return this.a;
        }


        Long getB() {
            return this.b;
        }

        private Character getSign() {
            if (OPERATIONS.contains(sign + "")) {
                return this.sign;
            }
            return null;
        }

        /**
         * Парсит в Long
         *
         * @param number строковое число
         * @return результат парсинга
         */
        private static Long convertInLong(String number) {
            try {
                return Long.parseLong(number);
            } catch (NumberFormatException e) {
                return null;
            }
        }

        /**
         * Возвращает результат бинарного выражения
         *
         * @param a    первое число
         * @param sign бинарный знак
         * @param b    второе число
         * @return результат вычисления
         */
        private String calculator(Long a, char sign, Long b) {
            String result = null;
            try {
                switch (sign) {
                    case '+':
                        result = MathInteger.addition(a, b) + "";
                        break;
                    case '-':
                        result = MathInteger.subtraction(a, b) + "";
                        break;
                    case '*':
                        result = MathInteger.multiply(a, b) + "";
                        break;
                    case '/':
                        result = MathInteger.division(a, b) + "";
                        break;
                    case '^':
                        result = MathInteger.pow(a, b) + "";
                        break;
                    case '%':
                        result = MathInteger.divisionEnd(a, b) + "";
                        break;
                }
                return result;
            } catch (ArithmeticException | NumberFormatException e) {
                return e.getMessage();
            }
        }
    }
}