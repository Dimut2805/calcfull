package ru.uds.mathFileAndScanner.directoryInterface;


import ru.uds.mathFileAndScanner.directoryClass.MathObject;

import java.util.ArrayList;

public interface Write {
    void writeResult(ArrayList<MathObject> arrayList);
}
