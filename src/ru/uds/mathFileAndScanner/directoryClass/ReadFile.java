package ru.uds.mathFileAndScanner.directoryClass;

import ru.uds.mathFileAndScanner.directoryInterface.Read;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Читает строки с условием из файла
 */
public class ReadFile implements Read {
    private String url;

    /**
     * Запрашивает адрес на файл
     *
     * @param url адрес файла
     */
    ReadFile(String url) {
        this.url = url;
    }

    /**
     * Читает строки с условием из указанного файла
     *
     * @return строка с условием
     */
    @Override
    public ArrayList read() {
        String string;
        ArrayList<MathObject> binaryExpressions = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(url))) {
            while ((string = bufferedReader.readLine()) != null) {
                binaryExpressions.add(new MathObject(string));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return binaryExpressions;
    }
}