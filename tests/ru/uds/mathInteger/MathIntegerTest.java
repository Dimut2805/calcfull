package ru.uds.mathInteger;

import org.junit.Assert;
import org.junit.Test;

public class MathIntegerTest {

    @Test(expected = NumberFormatException.class)
    public void multiply() {
        Assert.assertEquals(MathInteger.multiply(4, 4), 16);
        Assert.assertEquals(MathInteger.multiply(4, 0), 0);
        Assert.assertEquals(MathInteger.multiply(0, 4), 0);
        MathInteger.multiply(((long) Integer.MAX_VALUE) + 10, 4);
        MathInteger.multiply(4, ((long) Integer.MAX_VALUE) + 10);
        MathInteger.multiply(((long) Integer.MIN_VALUE) - 10, 4);
        MathInteger.multiply(4, ((long) Integer.MIN_VALUE) - 10);
        MathInteger.multiply(Integer.MAX_VALUE / 2, (Integer.MAX_VALUE / 2) + 2);
    }

    @Test(expected = ArithmeticException.class)
    public void division() {
        MathInteger.division(1,0);
        MathInteger.multiply(((long) Integer.MAX_VALUE) + 10, 4);
        MathInteger.multiply(4, ((long) Integer.MAX_VALUE) + 10);
        MathInteger.multiply(((long) Integer.MIN_VALUE) - 10, 4);
        MathInteger.multiply(4, ((long) Integer.MIN_VALUE) - 10);
        Assert.assertEquals(MathInteger.division(0,2), 0);
    }

    @Test
    public void addition() {
    }

    @Test
    public void subtraction() {
    }

    @Test
    public void pow() {
    }

    @Test
    public void divisionEnd() {
    }
}