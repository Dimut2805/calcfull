package ru.uds.mathProject;

import java.util.Scanner;

/**
 * Читает из консоли условие, считает, ответ выводит в консоль
 * <p>
 * author D. Utin 17IT18
 */
public class MainInputScanner {
    public static void main(String[] args) {
        outputScanner(calculator(inputString()));
    }

    /**
     * Решает бинарное выражение
     *
     * @param arguments аргументы условия
     * @return результат математических операций
     */
    private static String calculator(String[] arguments) {
        String string = "";
        try {
            switch (arguments[1]) {
                case "+":
                    string = String.valueOf(MathInteger.addition(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "-":
                    string = String.valueOf(MathInteger.subtraction(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "*":
                    string = String.valueOf(MathInteger.multiply(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "/":
                    string = String.valueOf(MathInteger.division(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "**":
                    string = String.valueOf(MathInteger.pow(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "%":
                    string = String.valueOf(MathInteger.divisionEnd(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
            }
        } catch (NumberFormatException | ArithmeticException e) {
            return "Ошибка! " + e.getMessage();
        } catch (Exception e) {
            return "Глобальная ошибка! " + e.getMessage();
        }
        return string;
    }

    /**
     * Выводит результат в консоль
     *
     * @param string ответ
     */
    private static void outputScanner(String string) {
        System.out.println(string);
    }

    /**
     * Читает условие из консоли
     *
     * @return читает условие из консоли
     * @throws NumberFormatException не верное условие
     */
    private static String[] inputString() throws NumberFormatException {
        String[] inputString = new Scanner(System.in).nextLine().split(" ");
        if (inputString.length != 3) throw new NumberFormatException("Неверный формат данных");
        return inputString;
    }
}