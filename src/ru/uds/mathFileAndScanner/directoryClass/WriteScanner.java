package ru.uds.mathFileAndScanner.directoryClass;

import ru.uds.mathFileAndScanner.directoryInterface.Write;

import java.util.ArrayList;


public class WriteScanner implements Write {
    /**
     * Пишет строки ответа в консоль
     *
     * @param resultBinaryExpressions массив строк ответа
     */
    @Override
    public void writeResult(ArrayList<MathObject> resultBinaryExpressions) {
        for (MathObject mathObject : resultBinaryExpressions) {
            System.out.println(mathObject.resultBinaryExpression());

        }
    }
}