package ru.uds.mathProJectFile;

import ru.uds.mathInteger.MathInteger;

import java.io.*;
import java.util.ArrayList;

/**
 * Читает условие из файла, считает, выводит результат решения в файл
 * <p>
 * author D. Utin 17IT18
 */
public class MainInputFile {
    public static void main(String[] args) {
        String SRC_INPUT = "src\\ru\\uds\\mathProjectFile\\input.txt",
                SRC_OUTPUT = "src\\ru\\uds\\mathProjectFile\\output.txt";
        writeFile(doArrayListForOutput(readFile(SRC_INPUT)));
    }

    /**
     * Перезаписывает значения списочного массива на результат
     *
     * @param arrayList смписочный массив с условием
     * @return списочный массив с результатами
     */
    private static ArrayList doArrayListForOutput(ArrayList<String> arrayList) {
        for (int i = 0; i < arrayList.size(); i++) {
            arrayList.set(i, calculator((arrayList.get(i) + "").split(" ")));
        }
        return arrayList;
    }

    /**
     * Подсчитывает результат условия
     *
     * @param arguments аргументы условия
     * @return результат математических вычислений
     */
    private static String calculator(String[] arguments) {
        String string = "";
        try {
            if (arguments.length != 3) throw new NumberFormatException("Неккоректный формат");
            switch (arguments[1]) {
                case "+":
                    string = String.valueOf(MathInteger.addition(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "-":
                    string = String.valueOf(MathInteger.subtraction(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "*":
                    string = String.valueOf(MathInteger.multiply(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "/":
                    string = String.valueOf(MathInteger.division(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "**":
                    string = String.valueOf(MathInteger.pow(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
                case "%":
                    string = String.valueOf(MathInteger.divisionEnd(Long.parseLong(arguments[0]), Long.parseLong(arguments[2])));
                    break;
            }
        } catch (NumberFormatException | ArithmeticException e) {
            return "Ошибка! " + e.getMessage();
        } catch (Exception e) {
            return "Глобальная ошибка! " + e.getMessage();
        }
        return string;
    }

    /**
     * Читает условия из файла
     *
     * @param url путь к файлу для чтения
     * @return математические условия
     */
    private static ArrayList<String> readFile(String url) {
        String string;
        ArrayList<String> arrayList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(url))) {
            while ((string = bufferedReader.readLine()) != null) {
                arrayList.add(string);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return arrayList;
    }

    /**
     * Пишет результаты в файл
     *
     * @param arrayList результаты вычислений
     */
    private static void writeFile(ArrayList arrayList) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src\\ru\\uds\\mathProjectFile\\output.txt", false))) {
            for (Object object : arrayList) {
                bufferedWriter.write(object + System.lineSeparator());
            }
        } catch (IOException e) {
            System.out.println("Ошибка при работе с файлом");
        }
    }
}