package ru.uds.mathProject;

/**
 * Реализует математические вычисления
 */
public class MathInteger {
    /**
     * Умножение
     *
     * @param a первый аргумент
     * @param b второй аргумент
     * @return результат вычисления
     */
    public static int multiply(long a, long b) {
        if (a > Math.abs(Integer.MAX_VALUE) ||
                b > Math.abs(Integer.MAX_VALUE) ||
                (a * b) > Math.abs(Integer.MAX_VALUE)) {
            throw new NumberFormatException("Переполнение");
        }
        return (int) a * (int) b;
    }

    /**
     * Деление
     *
     * @param a первое число
     * @param b второе число
     * @return результат вычисления
     */
    public static int division(long a, long b) {
        if (b == 0) {
            throw new ArithmeticException("Деление на 0");
        }
        if (a > Math.abs(Integer.MAX_VALUE) ||
                b > Math.abs(Integer.MAX_VALUE) ||
                (a % b) > Math.abs(Integer.MAX_VALUE)) {
            throw new NumberFormatException("Переполнение");
        }
        return (int) a / (int) b;
    }

    /**
     * Сложение
     *
     * @param a первое число
     * @param b второе число
     * @return результат вычисления
     */
    public static int addition(long a, long b) {
        if (a > Math.abs(Integer.MAX_VALUE) ||
                b > Math.abs(Integer.MAX_VALUE) ||
                (a + b) > Math.abs(Integer.MAX_VALUE)) {
            throw new NumberFormatException("Переполнение");
        }
        return (int) a + (int) b;
    }

    /**
     * Вычитание
     *
     * @param a первое число
     * @param b второе число
     * @return результат вычисления
     */
    public static int subtraction(long a, long b) {
        if (a > Math.abs(Integer.MAX_VALUE) ||
                b > Math.abs(Integer.MAX_VALUE) ||
                (a - b) > Math.abs(Integer.MAX_VALUE)) {
            throw new NumberFormatException("Переполнение");
        }
        return (int) a - (int) b;
    }

    /**
     * Возведение в степень
     *
     * @param a первое число
     * @param n второе число
     * @return результат вычисления
     */
    public static int pow(long a, long n) {
        if (n > Math.abs(31)) {
            throw new ArithmeticException("Переполнение");
        }
        if (n == 0 || a == 1) {
            return 1;
        }
        int valueA = (int) a;
        int valueN = (int) n;
        int result = valueA;
        for (int i = 1; i < valueN; i++) {
            result = result * valueA;
        }
        return result;
    }

    /**
     * Остаток от деления
     *
     * @param a первое число
     * @param b второе число
     * @return результат вычисления
     */
    public static int divisionEnd(long a, long b) {
        if (b == 0) {
            throw new ArithmeticException("Деление на 0");
        }
        if (a > Math.abs(Integer.MAX_VALUE) ||
                b > Math.abs(Integer.MAX_VALUE) ||
                (a % b) > Math.abs(Integer.MAX_VALUE)) {
            throw new NumberFormatException("Переполнение");
        }
        return (int) a % (int) b;
    }
}