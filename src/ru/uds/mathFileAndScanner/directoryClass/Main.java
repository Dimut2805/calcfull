package ru.uds.mathFileAndScanner.directoryClass;



/**
 * Реализует ввод-вывод, подсчет
 * <p>
 * author D. Utin 17IT18
 */
public class Main {
    final static String SRC_INPUT = "src\\ru\\uds\\mathFileAndScanner\\directoryFile\\input.txt",
            SRC_OUTPUT = "src\\ru\\uds\\mathFileAndScanner\\directoryFile\\output.txt";
    public static void main(String[] args) {
        new WriteScanner().writeResult(new ReadFile(SRC_INPUT).read());
    }
}